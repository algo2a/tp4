#ifndef TP3_FONCTION_H
#define TP3_FONCTION_H

//Déclaration des différent etats possible
enum Action {
    SUPPRESSION_TETE, AJOUT_QUEUE
};

//Déclaration des structures
struct Maillon {
    char *nom;
    Maillon *suivant;
    Maillon *precedent;
};

struct Historique {
    Maillon *derniereModif;
    Action derniereAction;
    Historique *suivant;
};

struct FileAttente {
    Maillon *tete;
    Maillon *queue;
    int longueur;
};

struct PileHistorique {
    Historique *tete;
};


//Déclarartion des fonction et procédure utilitaires
FileAttente *initialisationListeAttente();

Maillon *initialisationMaillon(char *, Maillon *);

Historique *initialisationHistorique(Maillon *derniereModif, Action derniereAction, Historique *suivant);

PileHistorique *initialisationPileHistorique();

void empilerHistorique(PileHistorique *pile, Historique *derniereAction);


//Déclaration des procédure du programme
void menu();

void ajouterPersQueue(char *, FileAttente *, PileHistorique *);

void retirerPersTete(FileAttente *, PileHistorique *);

void consulterPersTete(FileAttente *);

void calculerLongueurFile(FileAttente *);

void undo(FileAttente *, PileHistorique *);

#endif