#include <iostream>
#include "fonctions.h"

using namespace std;


////////////////////////
/*
 * Cette fonction permet d'initialiser une nouvelle liste d'attente.
 *
 *  ENTREE : RIEN
 *  SORTIE : Pointeur vers une structure FileAttente
 */
////////////////////////
FileAttente *initialisationListeAttente() {
    auto *file = (FileAttente *) malloc(sizeof(FileAttente));
    file->queue = nullptr;
    file->tete = nullptr;
    file->longueur = 0;
    return file;
}


////////////////////////
/*
 * Cette fonction permet d'initialiser un nouveau maillon.
 *
 *  ENTREE : RIEN
 *  SORTIE : Pointeur vers une structure Maillon
 */
////////////////////////
Maillon *initialisationMaillon(char *nom, Maillon *queueFile) {
    auto *maillon = (Maillon *) malloc(sizeof(Maillon));
    maillon->nom = nom;
    maillon->suivant = nullptr;
    maillon->precedent = queueFile;
    return maillon;
}


////////////////////////
/*
 * Cette fonction permet d'initialiser un nouvel historique. Ce dernier permet d'identifier unitairement
 * une action effectué sur la file :
 * - derniereAction décrit l'action.
 * - derniereModif est le maillon ajouté/supprimé.
 * - suivant est l'action suivante effectué sur la file dans l'ordre inverse chronologiquement
 *
 *  ENTREE : Pointeur vers une structure Maillon, Une Action, Pointeur vers une structure Historique
 *  SORTIE : Pointeur vers une structure Historique
 */
////////////////////////
Historique *initialisationHistorique(Maillon *derniereModif, Action derniereAction, Historique *suivant) {
    auto *historique = (Historique *) malloc(sizeof(Historique));
    historique->derniereModif = derniereModif;
    historique->derniereAction = derniereAction;
    historique->suivant = suivant;
    return historique;
}


////////////////////////
/*
 * Cette fonction permet d'initialiser une nouvelle pile d'historique
 *
 *  ENTREE : RIEN
 *  SORTIE : Pointeur vers une structure PileHistorique
 */
////////////////////////
PileHistorique *initialisationPileHistorique() {
    auto *pilehistorique = (PileHistorique *) malloc(sizeof(PileHistorique));
    pilehistorique->tete = nullptr;
    return pilehistorique;
}


////////////////////////
/*
 * Cette fonction permet d'ajouter en tete de la pile d'historique une structure de type historique
 *
 *  ENTREE : Un pointeur vers la pile, La structure Historique à ajouter
 *  SORTIE : RIEN
 */
////////////////////////
void empilerHistorique(PileHistorique *pile, Historique *derniereAction) {
    pile->tete = derniereAction;
}


////////////////////////
/*
 * Cette fonction permet de supprimer la structure de type historique en tete de la pile d'historique
 *
 *  ENTREE : Un pointeur vers la pile
 *  SORTIE : RIEN
 */
////////////////////////
void depilerHistorique(PileHistorique *pile) {
    pile->tete = pile->tete->suivant;
}


////////////////////////
/*
 * Cette procédure permet d'afficher le menu de selection a l'utilisteur
 *
 *  ENTREE : RIEN
 *  SORTIE : RIEN
 */
////////////////////////
void menu() {
    cout << endl << "//////////////////////// FILE D'ATTENTE ////////////////////" << endl;
    cout << "1 - Ajouter une personne en queue !" << endl;
    cout << "2 - Retirer une personne en tete !" << endl;
    cout << "3 - Consulter la personne en tete de file !" << endl;
    cout << "4 - Calculer la longueur de la file d'attente !" << endl;
    cout << "5 - Undo !" << endl;
    cout << "6 - Quitter !" << endl;
}


////////////////////////
/*
 * Cette procédure permet d'ajouter une personne en queue de la file d'attente
 *
 *  ENTREE : Pointeur vers le premier caractères de la chaine de caratère composant nom de la nouvelle personne,
 *              Pointeur vers la file d'attente actuelle
 *  SORTIE : RIEN
 */
////////////////////////
void ajouterPersQueue(char *nom, FileAttente *file, PileHistorique *pile) {

    if (nom == nullptr || file == nullptr) {
        cout << "Erreur, impossible d'ajouter une personne a la file d'attente : " << endl;
    } else {

        //Déclaration et initialisation d'un nouveau maillon
        Maillon *newMaillon = initialisationMaillon(nom, file->queue);

        //MAJ de l'historique
        auto *derniereAction = initialisationHistorique(newMaillon, AJOUT_QUEUE, pile->tete);
        empilerHistorique(pile, derniereAction);

        //Si la liste est vide
        if (file->tete == nullptr) {
            file->tete = newMaillon;
            file->queue = newMaillon;
        } else {
            file->queue->suivant = newMaillon;
            file->queue = newMaillon;
        }

        //Incrémentation de la longueur de la file
        file->longueur = file->longueur + 1;

        cout << "La persone ";
        //Affichage des 30 premiers caractères de la chaine de caractères
        int i = 0;
        while (i < 30 && nom[i] != '\0') {
            cout << nom[i];
            i++;
        }
        cout << " a bien ete ajoute a la file d'attente" << endl;

    }
}


////////////////////////
/*
 * Cette procédure permet de retirer la personne en tete de la file d'attente
 *
 *  ENTREE : Pointeur vers la file d'attente actuelle
 *  SORTIE : RIEN
 */
////////////////////////
void retirerPersTete(FileAttente *file, PileHistorique *pile) {

    //Test si la file d'attente existe et n'est pas vide
    if (file != nullptr && file->tete != nullptr) {

        //MAJ de l'historique
        auto *derniereAction = initialisationHistorique(file->tete, SUPPRESSION_TETE, pile->tete);
        empilerHistorique(pile, derniereAction);

        //Déclaration et initialisation d'un pointeur vers caractère afin de garder le nom de la personne supprimé
        char *nomPersSupr = file->tete->nom;

        //Décrémentation de la longueur de la file d'attente
        file->longueur = file->longueur - 1;

        cout << "La personne en tete qui possede le nom " << nomPersSupr << " a bien ete retirer de la file d'attente !"
             << endl;

        //Pointer le tete de la liste vers l'ancien 2ieme maillon de la file d'attente
        file->tete = file->tete->suivant;
        if (file->tete != nullptr) {
            file->tete->precedent = nullptr;
        }

    } else {
        cout << "Il n'y a plus personnes dans la file d'attente !" << endl;
    }
}


////////////////////////
/*
 * Cette procédure permet de consulter le nom de la personne en tete de la file d'attente
 *
 *  ENTREE : Pointeur vers la file d'attente actuelle
 *  SORTIE : RIEN
 */
////////////////////////
void consulterPersTete(FileAttente *file) {

    //Si la file existe et n'est pas vide
    if (file != nullptr && file->tete != nullptr) {
        cout << "Le nom de la personne qui est en tete de la file d'attente est : " << file->tete->nom;
    } else {
        cout << "Il n'y a plus personne dans la fille d'attente !";
    }
}


////////////////////////
/*
 * Cette procédure permet de consulter la longueur de la file d'attente
 *
 *  ENTREE : Pointeur vers la file d'attente actuelle
 *  SORTIE : RIEN
 */
////////////////////////
void calculerLongueurFile(FileAttente *file) {

    //Si la file existe et n'est pas vide
    if (file != nullptr && file->tete != nullptr) {
        cout << "Longueur file attente : " << file->longueur << endl;
    } else {
        cout << "La liste est vide" << endl;
    }
}


////////////////////////
/*
 * Cette procédure permet d'annuler la dernière action efectué sur la liste
 *
 *  ENTREE : Pointeur vers la file d'attente actuelle
 *  SORTIE : RIEN
 */
////////////////////////
void undo(FileAttente *file, PileHistorique *pile) {

    //Si la file existe
    if (file != nullptr && pile->tete != nullptr) {

        //Si la dernière action était une suppression en tete
        if (pile->tete->derniereAction == SUPPRESSION_TETE) {

            //Pointer le maillon supprimer comme précédent de la tete actuel
            if (file->tete != nullptr) {
                file->tete->precedent = pile->tete->derniereModif;
            }

            //Pointer la tete vers le maillon supprimer
            file->tete = pile->tete->derniereModif;

            //Incrémenter la longueur de la file
            file->longueur = file->longueur + 1;

            //Message de confirmation de l'anulation de la suppression
            cout << "La suppression de : " << pile->tete->derniereModif->nom << " en tete de file, a ete annule"
                 << endl;

            //MAJ historique
            depilerHistorique(pile);

        } else

            //Si la dernière action était un ajout en queue
        if (pile->tete->derniereAction == AJOUT_QUEUE) {

            //Si c'est le seul maillon de la file, alors faire pointer la tete vers null
            if (file->queue->precedent == nullptr) {
                file->tete = nullptr;
            }

            //La queue pointe vers le maillon précédent le dernier maillon actuel
            file->queue = file->queue->precedent;

            //S'il reste au moins un maillon de la file, alors suprésion du lien avec l'ancien dernier maillon
            if (file->queue != nullptr) {
                file->queue->suivant = nullptr;
            }

            //Décrémenter la longueur de la file
            file->longueur = file->longueur - 1;

            //Message de confirmation de l'anulation de l'ajout
            cout << "L'ajout de : " << pile->tete->derniereModif->nom << " en queue de file, a ete annule"
                 << endl;

            //MAJ historique
            depilerHistorique(pile);

        } else {
            cout << "Aucune action recente peut etre annulee" << endl;
        }
    } else {
        cout << "Aucune action recente peut etre annulee" << endl;
    }
}